#!/usr/bin/env python
"""

--------------------------------------
DDOS Restful API Via JSON WITH EXABGP and FLASK


I was having some issue with tinydb. You may have to chmod the file
so you can access it. Swagger is used for the API spec. Not perfect
and just a start.

jhenry6@gmail.com

"""
from tinydb import TinyDB, where, Query
from flask import Flask, request, render_template, abort
from flask_restful import Resource, Api
import sys
from flask_restful_swagger import swagger
from functools import wraps
app = Flask(__name__)
api = swagger.docs(Api(app), apiVersion='2', api_spec_url='/api/spec')
app.config['TRAP_BAD_REQUEST_ERRORS'] = True
db = TinyDB("/tmp/sail_ddos.json")
querier = Query()


def announce_message(message_type, source, source_port, destination, destination_port, protocol, action):
    if message_type.lower() == 'announce':
        announce_header ="announce flow route {\\n match {\\n "
    elif message_type.lower() == 'withdraw':
        announce_header ="withdraw flow route {\\n match {\\n "
    announce_source = ("source %s;\\n " % (source))
    announce_source_port = ("source-port %s;\\n " % (source_port))
    announce_destination = ("destination %s;\\n " % (destination))
    announce_destination_port = ("destination-port %s;\\n " % (destination_port))
    announce_protocol = ("protocol %s; \\n }\\n then {\\n " % (protocol))
    announce_action = ("%s;\\n }\\n }\\n }\\n\n" % (action))
    message = announce_header
    if source:
        message += announce_source
    if source_port:
        message += announce_source_port
    if destination:
        message += announce_destination
    if destination_port:
        message += announce_destination_port
    message += announce_protocol + announce_action
    return(message)

#This will read in information from the tinydb file of previous announced.
for entry in db.all():
    for x,y in entry.iteritems():
        try:
            source = entry['source']
        except KeyError:
            source = False
        try:
            source_port = entry['source-port']
        except KeyError:
            source_port = False
        try:
            destination_port = entry['destinaton-port']
        except KeyError:
            destination_port = False
        destination = entry['destination']
        protocol = entry['protocol']
        action = entry['action']
        typer = entry['type']
        message = (announce_message(typer, source, source_port, destination, destination_port, protocol, action))
        sys.stdout.write(message + '\n')
        sys.stdout.flush()

class ddos_messaging(Resource):
    "Send a call to create a DDOS FlowSpec Entry."
    @swagger.operation(notes="Send a call to create a DDOS FlowSpec Entry. You can look at Flowspec via Exabgp's Site to determine \
        the exact syntax.")
    def post(self):
        jsondicter = request.get_json(force=True)
        db.insert(jsondicter)
        for x,y in jsondicter.iteritems():
            try:
                source = jsondicter['source']
            except KeyError:
                source = False
            try:
                source_port = jsondicter['source-port']
            except KeyError:
                source_port = False
            try:
                destination_port = jsondicter['destinaton-port']
            except KeyError:
                destination_port = False

            destination = jsondicter['destination']
            protocol = jsondicter['protocol']
            action = jsondicter['action']
            typer = jsondicter['type']
            message = (announce_message(typer, source, source_port, destination, destination_port, protocol, action))
            if typer == 'withdraw':
                db.remove((querier.destination == jsondicter['destination']) & (querier.protocol == jsondicter['protocol']))
            sys.stdout.write(message + '\n')
            sys.stdout.flush()
        return (message)

class get_prefixes(Resource):
    "Get Existing Routes"
    @swagger.operation(notes="Gets All Existing Routes being blocked.")
    def get(self):
        return(db.all())


#<-------API RESOURCE ROUTING ---------->
api.add_resource(get_prefixes, '/ddos/get')
api.add_resource(ddos_messaging, '/ddos/message')
@app.route("/")
def sail_in():
    return render_template('readme.html')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')

