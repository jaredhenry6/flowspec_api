#Summary
This API allows you to talk to exabgp and send restful calls to announce, withdraw and get flowspec'd prefixes. You will of course need to be running exabgp. You can see the sample flowspec.ini on how I have set it up. To run the application.

```
#exabgp flowspec.ini
```

Use something as postman and make post to the API. 

```html
POST /ddos/message HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: application/json
Accept: application/json
Cache-Control: no-cache
Postman-Token: 744f16ef-6396-aeb4-b2f9-44bb81e2be92

{"type":"announce","destination":"5.5.5.5/32","destination-port":"22","protocol":"tcp","action":"discard"}
```
To withdraw the same:
```html
POST /ddos/message HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: application/json
Accept: application/json
Cache-Control: no-cache
Postman-Token: 744f16ef-6396-aeb4-b2f9-44bb81e2be92

{"type":"withdraw","destination":"5.5.5.5/32","destination-port":"22","protocol":"tcp","action":"discard"}
```

All flowspecs are written to a tinydb.

You can view all the items by doing the following:


http://127.0.0.1:5000/ddos/get

You will see:

```json
[
  {
    "action": "discard",
    "destination": "150.45.15.12/32",
    "protocol": "icmp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "140.45.15.12/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "167.45.15.12/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "167.45.15.13/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "167.45.15.14/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "167.45.15.15/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  },
  {
    "action": "discard",
    "destination": "167.45.15.16/32",
    "destination-port": "22",
    "protocol": "tcp",
    "type": "announce"
  }
]
```


```